package com.devcamp.s50.jbr3_70;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr370Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr370Application.class, args);
	}

}
