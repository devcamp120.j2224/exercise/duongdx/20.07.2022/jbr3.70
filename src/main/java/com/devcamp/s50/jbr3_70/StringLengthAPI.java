package com.devcamp.s50.jbr3_70;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StringLengthAPI {
    @CrossOrigin
    @GetMapping("/length")
    public int str(){
        String ThuCuoi = "cho bao nhieu yeu thuong di qua";
        ThuCuoi.length();

        System.out.println("chieu dai cua chuoi la:  " + ThuCuoi.length());
        return ThuCuoi.length() ;   
    }
}
